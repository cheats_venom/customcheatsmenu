#pragma once
#include <direct.h>
#include <stdio.h>
#include <Windows.h>
#include <psapi.h>
#include <intrin.h>
#include <vector>
#include "../Config.h"

#if IsRust

struct SETTINGS {

    int iConfig = 0;
    int oldiConfig = 0;

    struct {

        bool Enable = true;
        bool NPC = true;
        bool SaveTarget = true;
        bool AimLock = false;
        bool VisibilityCheck = true;
        bool IgnoreWounded = true;

        bool DrawFov = true;
        bool DrawCrossHair = false;
        bool DrawTarget = true;

        int DrawTargetType = 0;
        float DrawThickness = 1.f;

        int Predict = 0;
        int HitBox = 3;

        int FOV = 45;
        float min_time = 0.1f;
        float max_time = 0.7f;
        int MaxDistance = 600;

    }Aimbot;

    struct {
        bool Silent = false;
        int SilentHitChance = 100;
        int SilentHeadShotChance = 100;
        bool AttackOnMountable = false;
        bool RecoilControl = false;
        int RecoilScale = 10;
        bool SpreadControl = false;
        bool SwayControl = false;
        bool BulletSpeed = false;
        bool FullAuto = false;
        bool InstantEoka = false;
        bool ShootAir = false;
        bool ExtendedMelee = false;
        bool SuperBow = false;
        bool GravityMultiplier = false;
        bool NoFall = false;
        bool Spiderman = false;
        bool HighJump = false;

        bool AdminFlag = false;
        bool AdminByHold = false;
        bool FovChanger = false;
        float FovChangerValue = 150.f;
        bool ThirdPerson = false;
        bool nightSky = false;
        bool night_mode = false;
        float time = 24.f;

        bool ZoomFov = false;
        float ZoomFovValue = 30.f;
    }Misc;

    struct {
        struct {
            bool Enable = true;
            int MaxDistance = 800;
            float FontSize = 16.f;

            bool TeamCheck = true;
            bool RemoveSleeping = false;

            bool NickName = true;
            bool HeadCircle = true;

            bool OutScreen = true;
            int OutScreenType = 0;
            int OutScreenFov = 15;
            float OutScreenThickness = 2.f;
            int OutScreenSize = 10.f;

            bool Box = true;
            int BoxType = 0;
            float BoxThickness = 1.f;

            bool Health = true;
            int HealthType = 0;
            float HealthThickness = 4.f;

            bool Lines = true;
            int LinesPosition = 0;
            float LinesThickness = 1.f;

            bool Distance = true;
            bool Weapon = true;

            bool Skeleton = true;
            float SkeletonThickness = 1.f;

            bool HotBar = true;
            int HotBarX = 300;
            int HotBarY = 20;
            float HotBarFontSize = 16.f;

            bool Chams = true;

        }Players;

        struct {
            bool Enable = false;
            bool Type[3] = { true,true, true };
            bool Name = true;
            bool HeadCircle = true;

            int MaxDistance = 800;
            float FontSize = 16.f;

            bool Box = true;
            int BoxType = 0;
            float BoxThickness = 1.f;

            bool Health = true;
            int HealthType = 0;
            float HealthThickness = 4.f;

            bool Lines = true;
            int LinesPosition = 0;
            float LinesThickness = 1.f;

            bool Distance = true;

            bool Skeleton = true;
            float SkeletonThickness = 1.f;

        }NPC;

        struct {
            bool Enable = true;
            int MaxDistance = 800;
            float FontSize = 16.f;

            bool Corpse = true;
            bool CorpseType[2] = { true,true };
            bool Ore = true;
            bool OreType[3] = { true,true,false };
            bool Hemp = true;
            bool WorldDrop = true;
            bool WorldType[14] = { true,true,true,true,true,true,true,true,true,true,true,true,true,true };
            bool AirDrop = true;
            bool Traps = true;
            bool TrapsType[7] = { true,true,false,false,false,false,false };
            bool Vehicles = true;
            bool VehiclesType[9] = { true,true,false,false,false,false,false, false, false };
            bool Animals = true;
            bool AnimalsType[5] = { true,true,false,false,false };
            bool Cupboard = true;
            bool Crate = true;
            bool CrateType[5] = { true,true,false,false,false };
            bool Barrel = true;
            bool BarrelType[3] = { true,true,false };
        }Items;

        struct {
            bool Enable = false;
            int PositionX = 10;
            int PositionY = 30;
            int Size = 200;
            int Zoom = 10;

            bool VisibleColor = true;
            bool ClosestColor = true;
            int ClosestDistance = 10;
            int MaxDistance = 150;
        }Radar;
    }Visuals;

    struct {

        struct {
            int HoldPrimary = VK_LBUTTON;
            int HoldSecondary = VK_RBUTTON;
            int HoldNPC = VK_XBUTTON1;
            int SwitchToHead = VK_SHIFT;
            int Toggle = VK_F6;
        }Aimbot;

        struct {
            int TogglePlayers = VK_F7;
            int ToggleNPC = VK_F10;
            int ToggleItems = VK_F8;
            int ToggleRadar = VK_F9;
        }Visuals;

        int Menu = VK_INSERT;
        int AdminFlag = VK_F3;
        int AdminFlagHold = VK_F5;
        int ZoomFovHold = VK_XBUTTON2;

    }Keys;

    struct {
        struct {
            float Fov[3] = { 0.f, 1.f, 0.f };
            float CrossHair[3] = { 1.f, 0.f, 0.f };
            float Target[3] = { 1.f, 1.f, 0.f };
        }Aimbot;

        struct {
            float Visible[3] = { 1.f, 1.f, 1.f };
            float NonVisible[3] = { 1.f, 0.f, 0.f };
            float Distance[3] = { 1.f, 1.f, 1.f };
            float NickName[3] = { 1.f, 1.f, 1.f };
            float Weapon[3] = { 1.f, 1.f, 1.f };
        }Players;

        struct {
            float Visible[3] = { 1.f, 1.f, 1.f };
            float NonVisible[3] = { 1.f, 0.f, 0.f };
            float Distance[3] = { 1.f, 1.f, 1.f };
            float Name[3] = { 1.f, 1.f, 1.f };
        }NPC;

        struct {
            float Corpse[3] = { 1.f, 0.f, 0.f };
            float Ore[3] = { 1.f, 1.f, 1.f };
            float Hemp[3] = { 1.f, 1.f, 1.f };
            float WorldDrop[3] = { 1.f, 1.f, 1.f };
            float AirDrop[3] = { 1.f, 1.f, 1.f };
            float Traps[3] = { 1.f, 1.f, 1.f };
            float Vehicles[3] = { 1.f, 1.f, 1.f };
            float Animals[3] = { 1.f, 1.f, 1.f };
            float Cupboard[3] = { 1.f, 1.f, 1.f };
            float Crate[3] = { 1.f, 1.f, 1.f };
            float Barrel[3] = { 1.f, 1.f, 1.f };
        }Items;

        struct {
            float Enemies[3] = { 1.f, 0.f, 0.f };
            float Visible[3] = { 0.f, 1.0f, 0.f };
            float Closest[3] = { 1.f, 1.0f, 0.f };
        }Radar;
    }Colors;

    struct {
        int Language = 0;
        bool FeaturesDefinition = true;
        int FontStyle = 0;
    }Configs;
};

#endif